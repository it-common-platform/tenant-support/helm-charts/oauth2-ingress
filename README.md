
Oauth2-ingress
===========

A Helm chart for oauth2-ingress


Using [oauth2-proxy](https://code.vt.edu/it-common-platform/tenant-support/helm-charts/oauth2-ingress), setup an ingress to authenticate users.

**Important**: You must have an instance of oauth2-proxy running in order to use this chart.

## Certificate & DNS

This chart will generate a LetsEncrypt certificate and Ingress using the provided `certificate.commonName`. You are responsible for setting up DNS for this name.

## Example

```
apiVersion: source.toolkit.fluxcd.io/v1
kind: HelmRepository
metadata:
  name: oauth2-ingress
spec:
  url: https://code.vt.edu/api/v4/projects/20370/packages/helm/stable
  interval: 1h
  secretRef: 
    name: my-helm-chart-secret
---
apiVersion: helm.toolkit.fluxcd.io/v2
kind: HelmRelease
metadata:
  name: my-app
spec:
  targetNamespace: my-namespace
  serviceAccountName: flux
  chart:
    spec:
      sourceRef:
        kind: HelmRepository
        name: oauth2-ingress
      chart: oauth2-ingress
      version: 0.1.1
  install:
    remediation: 
      retries: 4
  upgrade:
    remediation:
      retries: 4
  interval: 1h
  values:
    certificate: 
      commonName: "myapp.mydept.vt.edu" 

    upstream:
      service: my-service
      port: 1234

    oauth2ProxyUrl: https://my-oauth2-proxy.mydept.vt.edu
    allowedGroups:
      - my.group1
      - my.group2
```

## Development

When developing this chart, if any change is made to `values.yaml`, update the README by doing the following:

```
pip install frigate
frigate gen --no-credits . > ./README.md
git commit -am "Update README"; git push origin <my-branch>
```

## Configuration

The following table lists the configurable parameters of the Oauth2-ingress chart and their default values.

| Parameter                | Description             | Default        |
| ------------------------ | ----------------------- | -------------- |
| `nameOverride` | Override the default chart name | `""` |
| `fullnameOverride` | Override the full name | `""` |
| `certificate.commonName` | **required** The DNS name for oath2-proxy (must be less than 64 bytes) | `""` |
| `certificate.alternateNames` | Any additional DNS names you need | `[]` |
| `upstream.service` | **required** The upstream service this ingress should send traffic to | `""` |
| `upstream.port` | **required** The port of the service | `""` |
| `oauth2ProxyUrl` | **required** The OAuth2 Proxy | `""` |
| `allowedGroups` | Optional list of groups to restrict access to | `[]` |





